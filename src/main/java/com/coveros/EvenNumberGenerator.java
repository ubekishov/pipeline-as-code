package com.coveros;

import java.util.Random;

public class EvenNumberGenerator {
	private EvenNumberGenerator()
	{
		//do nothing
	}
	public static int generateRandomEven(){
		Random randomGenerator = new Random();
		int result = randomGenerator.nextInt();
		if (result%2!=0){
			result++;
		}
		return result;
	}
	public static int generateRandomOdd(){
		Random randomGenerator = new Random();
		int result = randomGenerator.nextInt();	
		if (result%2==0){
			result++;
		}
		return result;
	}
}
