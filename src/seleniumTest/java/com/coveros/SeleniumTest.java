package com.coveros;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;;

public class SeleniumTest {
	@Test
	public void demoTest()  throws InterruptedException {
		String URL=new String("http://localhost:8090/demo");
		//WebDriver driver = new FirefoxDriver();
		WebDriver driver = new HtmlUnitDriver();
		
		driver.get(URL);
		System.out.println("Successfully opened the website" + URL);
		
        WebElement element = driver.findElement(By.tagName("h2"));
        String result = element.getText();
        assertEquals("Hello World! The even number is:",result);
		//Wait for 5 Sec
		Thread.sleep(5);
		
        // Close the driver
        driver.quit();
    }
}
