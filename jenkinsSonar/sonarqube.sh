#!/bin/bash

#sed -i 's/8080/8090/g' /etc/tomcat8/server.xml
#usermod -aG tomcat8 jenkins
SONARQUBE_HOME=/opt/sonar

# Update conf properties of Sonar with username and postgresql
echo "sonar.jdbc.username=sonar" >> $SONARQUBE_HOME/conf/sonar.properties
echo "sonar.jdbc.password=sonar" >> $SONARQUBE_HOME/conf/sonar.properties
echo "sonar.jdbc.url=jdbc:postgresql://localhost:5432/sonar" >> $SONARQUBE_HOME/conf/sonar.properties

#create user sonarqube and database sonar
/etc/init.d/postgresql start
su postgres -c "psql --command \"CREATE USER sonar with password 'sonar';\" "
su postgres -c "psql --command \"CREATE DATABASE sonar ENCODING 'UTF8' TEMPLATE=template0;\" "
su postgres -c "psql --command \"ALTER DATABASE sonar OWNER TO sonar;\" "
/etc/init.d/postgresql stop
